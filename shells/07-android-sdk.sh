#!/usr/bin/env bash

echo " 📦  Android SDK"

USR_LOCAL_BIN_PATH="/usr/local/bin"
ANDROID_BREW_CASK_INSTALL_PATH="${USR_LOCAL_BIN_PATH}/android"
SDKMANAGER_BREW_CASK_INSTALL_PATH="${USR_LOCAL_BIN_PATH}/sdkmanager"
# add following parameter at the end of sdkmanager command to use a specified proxy
#SDKMANAGER_PROXY_CONF="--proxy=http --proxy_host=proxy.my.company.com --proxy_port=8080"

ending_android_sdk_install() {
  ANDROID_SDK_DIR_AND_VERSION="in directory '${ANDROID_BREW_CASK_INSTALL_PATH}', SDK manager installed in directory '${SDKMANAGER_BREW_CASK_INSTALL_PATH}' in version '$(sdkmanager --version)'"
  print_info "SDK infos"
  print_info "ANDROID_HOME="${ANDROID_HOME}
  print_info "Accept all licenses ..."
  echo y | sdkmanager --licenses
  sdkmanager --list
}

if [[ ! -f $(which android) ]] &&
   ! test -d "${ANDROID_BREW_CASK_INSTALL_PATH}" &&
   ! test -d "${SDKMANAGER_BREW_CASK_INSTALL_PATH}"
then
  print_info_installing
  # Install ant/maven/gradle
  brew install ant
  brew install maven
  brew install gradle
  # Install SDK
  brew cask install android-sdk
  check_previous_command_exec "Android SDK installation"
  # Install platform-tools
  print_info "Installing platform-tools ..."
  sdkmanager "platform-tools"
  check_previous_command_exec "Android SDK platform-tools installation"
  # Install platforms
  PACKAGE_SDK_PLATFORMS_VERSION="$(sdkmanager --list --verbose 2> /dev/null | grep "platforms;android-.." | tail -1 | sed "s/platforms;//g")"
  print_info "Installing platforms;${PACKAGE_SDK_PLATFORMS_VERSION} ..."
  sdkmanager "platforms;${PACKAGE_SDK_PLATFORMS_VERSION}"
  check_previous_command_exec "Android SDK platforms installation"
  # Install build-tools
  PACKAGE_SDK_BUILD_TOOLS_VERSION="$(sdkmanager --list --verbose 2> /dev/null | grep "build-tools;" | grep -v "build-tools;.*-" | tail -1 | sed "s/build-tools;//g")"
  print_info "Installing build-tools;${PACKAGE_SDK_BUILD_TOOLS_VERSION} ..."
  sdkmanager "build-tools;${PACKAGE_SDK_BUILD_TOOLS_VERSION}"
  check_previous_command_exec "Android SDK build-tools installation"
  # Ending install
  print_info "Set ANT_HOME env var to '$(which ant)' ..."
  set_env_var ANT_HOME "$(which ant)"
  print_info "Set MAVEN_HOME env var to '$(which mvn)' ..."
  set_env_var MAVEN_HOME "$(which mvn)"
  print_info "Set GRADLE_HOME env var to '$(which gradle)' ..."
  set_env_var GRADLE_HOME "$(which gradle)"
  print_info "Set ANDROID_HOME env var ..."
  set_env_var ANDROID_HOME "/usr/local/share/android-sdk"
  print_info "Set PATH for android env ..."
  export PATH=$ANDROID_HOME/tools:$PATH
  export PATH=$ANDROID_HOME/platform-tools:$PATH
  export PATH=$ANDROID_HOME/build-tools/$PACKAGE_SDK_BUILD_TOOLS_VERSION:$PATH
  ending_android_sdk_install
  print_success_completed "Android SDK install ${ANDROID_SDK_DIR_AND_VERSION}"
else
  ending_android_sdk_install
  print_success "Android SDK already installed ${ANDROID_SDK_DIR_AND_VERSION}"
fi
