[`EN`](https://gitlab.com/MaxTod/macos-setup-ci) [`FR`](https://gitlab.com/MaxTod/macos-setup-ci/blob/master/README.fr.md)
# Installation PIC sur MacOS

Installation et configuration d'une PIC sur MacOS pour des applications mobiles (Android et iOS).


## Informations
Ici va vous être décrit la structure et le fonctionnement général des scripts mis à disposition.

### Structure
La structure est la suivante :

	.
	+-- core
	   +-- Brewfile_core                              (fichier des dépendances brew nécessaires à bundler)
	   +-- Brewfile_utils                             (fichier des dépendances brew utilitaires pouvant être ajoutées)
	   +-- config_folder.xml                          (fichier d'exemple de configuration pour un 'folder' Jenkins)
	   +-- config_pipeline_mobile_parallel_stages.xml (fichier d'exemple de configuration pour un pipeline mobile avec stages parallélisés)
	   +-- config_pipeline_mobile_parallel_steps.xml  (fichier d'exemple de configuration pour un pipeline mobile avec steps parallélisés)
	   +-- config_pipeline_test_ci.xml                (fichier d'exemple de configuration pour un pipeline de test de l'environnement la PIC)
	   +-- functions.sh                               (fonctions utilitaires)
	   +-- jenkins_default.yaml                       (fichier d'exemple de configuration Jenkins par défaut à charger via le plugin 'configuration as code')
	   +-- jenkins_plugins                            (liste des plugins Jenkins installés par les scripts)
	   +-- jenkins_tools.sh                           (fonctions utilitaires pour Jenkins)
	   +-- npm_packages                               (liste des packages npm installés par les scripts)
	+-- shells
	   +-- 00-env.sh                                  (préparation de l'environnement d'installation)
	   +-- 01-homebrew.sh                             (installation / vérification / mise à jour d'homebrew et de ses outils)
	   +-- 02-xcode.sh                                (installation de Xcode Command Line Tools et de Xcode)
	   +-- 03-git.sh                                  (installation de Git)
	   +-- 04-node.sh                                 (installation de Node (avec NPM))
	   +-- 05-npm.sh                                  (installation / vérification / mise à jour de NPM et de ses différents packages)
	   +-- 06-java.sh                                 (installation et configuration du JDK 1.8)
	   +-- 07-android-sdk.sh                          (installation et configuration du SDK android et installation des différents packages nécessaires via le sdkmanager)
	   +-- 08-haxm.sh                                 (installation et configuration de Haxm)
	   +-- 09-google-chrome.sh                        (installation de Google Chrome)
	   +-- 10-ruby-RVM.sh                             (installation, configuration et initialisation de RVM puis installation et initialisation de la version latest de Ruby)
	   +-- 11-fastlane.sh                             (installation / mise à jour de Fastlane)
	   +-- 12-jenkins.sh                              (installation de Jenkins puis start en tant que service (via brew))
	   +-- 50-utilities.sh                            (installation des utilitaires via le Brewfile_utils et check de l'environnement brew et cask)
	   +-- 51-sublime-text.sh                         (installation de Sublime Text 3)
	   +-- 52-zsh.sh                                  (installation de Ho-My-Zsh)
	+-- installCI                                     (script d'installation complète de la PIC, voir fonctionnement ci-après)
	+-- setupCI                                       (script de configuration de la PIC, voir fonctionnement ci-après)
	+-- uninstallCI                                   (script indépendant de désinstallation complète de la PIC, voir fonctionnement ci-après)

### Fonctionnement général
#### installCI
Ce script est un wrapper pour tous les scripts du répertoire **shells**, il va préconfigurer et exporter les fonctions et les variables d'environnement nécessaires à l'installation **(dont la configuration du proxy)**. Une authentification sera demandée pour être certain d'avoir les droits `sudo` à disposition dans les sous-shells. Après confirmation du lancement de l'installation, les sous-shells du répertoire **shells** seront tous lancés dans l'ordre.

Si une commande d'installation pricipale ne s'est pas déroulé correctement, alors le script principal s'arrêtera et loggera une erreur sans poursuivre l'installation.
#### Sous-shells
##### 00-env
Préparation de l'environnement d'installation.
Ce script va controler que les différents fichiers de profils existent (sinon il va les créer) et sont éditables. Si un des fichiers de profil nécessaire à l'install n'est pas éditable alors ce script retournera une erreur et l'installation sera stoppée.
##### 01-homebrew
Homebrew est un gestionnaire de packages très répendu sous macOS, vous trouverez plus d'information sur [le site de homebrew](https://brew.sh/index_fr).

Ce script va installer homebrew si ce n'est pas déjà fait, sinon il va nettoyé l'install courante, vérifier l'intégrité et rechercher des mises à jour pour lui et pour toutes les formulae installées.

Lors de la première installation, le fichier `core/Brewfile_core` est chargé en tant que bundle et va installer les outils nécessaires à Homebrew mais aussi les dépendances suivantes :
1. [`trash`](https://formulae.brew.sh/formula/trash) : permet de placer dans la corbeille l'app de son choix (utilisé pour désinstaller Xcode)
2. [`mas`](https://formulae.brew.sh/formula/mas) : met à disposition un certain nombre d'utilitaires (CLI) pour la gestions de l'AppStore (utilisé pour installer Xcode)
3. [`Xcode`](https://itunes.apple.com/fr/app/xcode/id497799835?mt=12) : outil de développement pour les suites Apple (nécessaire pour déployer des applications sur l'AppStore) 

##### 02-xcode
Ce script va installer Xcode Command Line Tools si nécessaire puis configurer Xcode en modifiant l'active directory, ce qui permet d'utiliser la commande `xcodebuild` et en accepter les conditions de license. Si Xcode n'a pas été installé lors du bundling du Brewfile_core, alors l'installation sera stoppée.
##### 03-git
Ce script va installer Git si nécessaire en tant que formulae Homebrew.
##### 04-node
Ce script va installer Node (avec NPM) si nécessaire en tant que formulae Homebrew.
##### 05-npm
Ce script va mettre à jour NPM si nécessaire puis installer tous les packages nécessaires depuis le fichier `core/npm_packages`. Si ces packages sont déjà installés, on recherchera les mises à jour. Si NPM n'est pas installé alors ce script retournera une erreur et l'installation sera stoppée.
##### 06-java
Ce script va désinstaller toutes les versions de java installées via `brew cask` puis installer java 8 via `brew cask`. Il va ensuite chercher à valoriser corerctement la variable d'environnement `JAVA_HOME` dans le `~/.bash_profile` si ce n'est pas déjà fait.
##### 07-android-sdk
Ce script va installer le SDK android si nécessaire avec les dépendances suivantes via le `sdkmanager` :
1. `platform-tools`
2. `platforms` en dernière version directement via le `sdkmanager`
3. `build-tools` en dernière version directement via le `sdkmanager`

Si le SDK android doit être installé, la variable d'environnement `ANDROID_HOME` sera positionné dans le `~/.bash_profile`.
##### 08-haxm
Ce script va installé Haxm si nécessaire et positionner la variable d'environnement `INTEL_HAXM_HOME` dans le `~/.bash_profile`.
##### 09-google-chrome
Ce script va installer Google Chrome si nécessaire. Google Chrome est indispensable pour la PIC car la plupart des projets mobiles l'utilisent en mode Headless pour lancer leurs tests unitaires (voir documentation karma).
##### 10-ruby-RVM
Ce script va installer RVM si nécessaire, initialiser l'environnement RVM pour le shell courant, ajouter aux différents fichiers de profils (`~/.bash_profile`, `~/.profile` ...) l'initialisation de RVM pour pouvoir prendre en compte les futures versions de Ruby qui seront installées. Ensuite, Ruby sera installé ou mis à jour si nécessaire en latest version.

**ATTENTION : pour pouvoir utiliser les versions de Ruby installées par RVM, il faut toujours utiliser un terminal `bash`. Ce qui signifie que tous les script DSL ou pipeline Jenkins devront prendre en compte ce point pour utiliser correctement Fastlane qui nécessite une version récente de Ruby.**
##### 11-fastlane
Ce script va installer ou mettre à jour [Fastlane](https://fastlane.tools/) si nécessaire.
##### 12-jenkins
Ce script va installer jenkins-lts si nécessaire en tant que formulae Homebrew. Il va ensuite démarrer l'instance Jenkins en tant que service brew avant de fournir les commandes disponnibles pour les services brew ainsi que la liste des services brew (si l'install est faite sur une machine vierge alors on ne devrait voir que le service jenkins-lts).
##### 50-utilities
Ce script va installer les utilitaires depuis le Brewfile_utils, vous pouvez y ajouter ce dont vous avez besoin qui n'est pas obligatoire pour la PIC.
##### 51-sublime-text
Ce script va demander si vous souhaitez installer Sublime Text 3 et l'installer si la réponse est oui.
##### 52-zsh
Ce script va demander si vous souhaitez installer Ho-My-Zsh et l'installer si la réponse est oui.

#### setupCI
Ce script est indépendant, in n'utilise que le contenu de répertoire **core** et non celui du répertoire **shells**. De la même façon que le script `installCI`, il va préconfigurer et exporter les fonctions et variables d'environnement nécessaires à la configuration **(dont la configuration du proxy)**. Une authentification sera demandée pour être certain d'avoir les droits `sudo` à disposition lors de la configuration. Après confirmation, la configuration de la PIC débutera dans l'ordre suivant :
1. Initialisation de la conf Jenkins
    1. Création et valorisation du fichier `lastExecVersion`
    2. Création et valorisation du fichier `locationConfiguration` par défaut à `http://localhost:8080/`. Le script peut être modifié pour passer une autre valeur à la fonction correspondante
    3. Modification de l'`installStateName` de NEW à RUNNING
    4. Valorisation de la variable d'environnement `JENKINS_URL` à la valeur par défaut `http://localhost:8080/` récupéré dans le fichier `locationConfiguration`
    5. Export de la variable d'environnement `JENKINS_URL`
    6. Positionnement du compte admin qui sera utilisé par `jenkins_cli` via les variable d'environnement `JENKINS_USERNAME` et `JENKINS_PASSWORD`
2. Demande de configuration du proxy, création du fichier `proxy.xml` au niveau du `JENKINS_HOME` si nécessaire
3. Installation des plugins Jenkins dans l'ordre du fichier `core/jenkins_plugins`
4. Demande d'ajout de configuration Jenkins globale via le plugin **configuration as code**, utilisation de la configuration par défaut `core/jenkins_default.yaml` ou d'une autre configuration dont il sera demandé le path
5. Ajout du folder Jenkins `Test CI CD` via le fichier `config_folder.xml`
6. Ajout du pipeline `check_env` via le fichier `config_pipeline_test_ci.xml` dans le folder Jenkins `Test CI CD`
7. Ajout du pipeline `pipe_mobile_v1` via le fichier `config_pipeline_mobile_parallel_steps.xml` à la racine du Jenkins

#### uninstallCI
Ce script est indépendant, il n'utilise que le contenu du répertoire **core** et non celui de répertoire **shells**. De la même façon que le script `installCI`, il va préconfigurer et exporter les fonctions et variables d'environnement nécessaires à la désinstallation **(dont la configuration du proxy)**. Une authentification sera demandée pour être certain d'avoir les droits `sudo` à disposition lors de la désinstallation. Après confirmation, le **service jenkins-lts** est stoppé puis toutes la PIC est déinstallé dans l'ordre suivant :
1. Jenkins
2. NPM packages
3. Dépendances `cask` du Brewfile_core
4. Dépendances `mas` du Brewfile_core
5. Java 8
6. Android SDK (avec dépendances sdkmanager)
7. Haxm
8. Google Chrome
9. RVM (avec Ruby)
10. Dépendances Homebrew (depuis Brewfile_core puis **git** et **node** si installés via brew)
11. Fastlane

L'environnement est nettoyé tout au long du processus de désinstallation (`.bashrc`, `.bash_profile`, `.profile` ...).

## Prérequis
Les différents scripts mis à disposition ici sont prévus pour fonctionner à partir de la version **macOS Mojave 10.14.4**, ils ont cependant été testés sous **macOS High Sierra 10.13.6**, il se peut cependant que certains outils ne se comportent pas correctement sous une version antérieurs à **Mojave**.

Certaines commandes nécessitent des droits administrateurs pour être lancées, c'est pourquoi les scripts `installCI`, `setupCI` et `uninstallCI` commencent par demander les droits `sudo`.

Il est conseillé d'utiliser ces scripts depuis un terminal bash loggé avec un compte possédant ces droits, sans quoi l'installation ne pourra pas être effectuée correctement.

## Utilisation
La commande suivante va créer un répertoire dédié à l'installation de la PIC, cloner le répo dans ce répertoire puis s'y positionner.
Après avoir supprimé le répertoire `.git/` le script `installCI` sera lancé et l'installation débutera.

`mkdir ~/.installCI && git clone https://gitlab.com/MaxTod/macos-setup-ci.git/ ~/.installCI && cd ~/.installCI && rm -rf .git/ && bash installCI`

Vous pouvez aussi lancer mauellement les script d'installation, de configuration et de désinstallation comme suit, après avoir clonné ce répo dans le répertoire `~/.installCI`
### Installation
`bash ~/.installCI/installCI`
### Configuration
**ATTENTION : l'installation et la configuration de l'instance jenkins nécessite le maintien de la configuration as code via le fichier `core/jenkins_default.yaml` ainsi que la liste ordonnée des plugins jenkins via le fichier `core/jenkins_plugins`. Merci de mettre à jour les valeurs nécessaires dans le fichier `core/jenkins_default.yaml` et de contrôler l'intégrité de la liste des plugins.**

`bash ~/.installCI/setupCI`
### Désinstallation
`bash ~/.installCI/uninstallCI`
