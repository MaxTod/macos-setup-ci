#!/usr/bin/env bash

echo " 📦  Homebrew"

ending_homebrew_install() {
  HOMEBREW_DIR_AND_VERSION="($(which brew)) in version $(brew --version)"
}

if [[ ! -f $(which brew) ]]
then
  print_info_installing
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  brew bundle --file="$ROOT_DIR/core/Brewfile_core"
  check_previous_command_exec "Homebrew bundling"
  brew cleanup && brew upgrade && brew update && brew doctor -v && brew cask doctor
  ending_homebrew_install
  print_success_completed "Homebrew install ${HOMEBREW_DIR_AND_VERSION}"
else
  ending_homebrew_install
  print_info "Homebrew already installed ${HOMEBREW_DIR_AND_VERSION}"
  print_info "Check homebrew install integrity and update it ..."
  brew cleanup && brew upgrade && brew update && brew doctor -v && brew cask doctor
  print_success_completed "Homebrew checkup"
fi
