#!/usr/bin/env bash

echo " 📦  Google Chrome"

GOOGLE_CHROME_INSTALL_PATH="/Applications/Google Chrome.app"

if ! test -d "${GOOGLE_CHROME_INSTALL_PATH}"
then
  print_info_installing
  brew cask install google-chrome
  check_previous_command_exec "Google Chrome installation"
  print_success_completed "Google Chrome install in directory '${GOOGLE_CHROME_INSTALL_PATH}'"
else
  print_success "Google Chrome already installed in directory '${GOOGLE_CHROME_INSTALL_PATH}'"
fi
