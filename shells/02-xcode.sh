#!/usr/bin/env bash

echo " 📦  XCode"

ending_xcode_clt_install() {
  XCODE_CLT_DIR_AND_VERSION="($(xcode-select --print-path)) in version '$(xcode-select --version)'"
}

init_xcode_intall() {
  xcodebuild -version &> /dev/null
  XCODEBUILD_STATUS=$?
}

ending_xcode_install() {
  XCODE_DIR_AND_VERSION="($(which xcodebuild)) in version : $(xcodebuild -version)"
}

if ! type xcode-select >&- &&
   ! xcode-select --print-path &> /dev/null
then
  print_info_installing
  # create the placeholder file that's checked by CLI updates' .dist code in Apple's SUS catalog
  touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
  # find the CLI Tools update
  PROD=$(softwareupdate -l | grep "\*.*Command Line" | head -n 1 | awk -F"*" '{print $2}' | sed -e 's/^ *//' | tr -d '\n')
  # install it
  softwareupdate -i "$PROD" --verbose
  check_previous_command_exec "Xcode Command Line Tools installation"
  rm /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
  ending_xcode_clt_install
  print_success_completed "XCode install ${XCODE_CLT_DIR_AND_VERSION}"
else
  ending_xcode_clt_install
  print_success "XCode Command Line Tools already installed ${XCODE_CLT_DIR_AND_VERSION}"
fi

init_xcode_intall
if [[ -f $(which xcodebuild) ]] && test ${XCODEBUILD_STATUS} -eq 0
then
  print_info "Reset Xcode active developer directory ..."
  sudo xcode-select -r
  xcode-select -p
  print_info "Accept the Xcode license agreements ..."
  sudo xcodebuild -license accept
  ending_xcode_install
  print_success "XCode installed ${XCODE_DIR_AND_VERSION}"
else
  print_error "Xcode" "not installed, please install it by bundle Brewfile_core or directly from AppStore"
  exit 1
fi
