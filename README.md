[`EN`](https://gitlab.com/MaxTod/macos-setup-ci) [`FR`](https://gitlab.com/MaxTod/macos-setup-ci/blob/master/README.fr.md)
# CICD installation on MacOS

Installation and configuration of a CICD platform on MacOS for mobile applications (Android and iOS).


## Information
Here will be described structure and general operation of scripts made available.

### Structure
Structure is as follows:

	.
	+-- core
	   +-- Brewfile_core                              (brew dependency file needed to bundle)
	   +-- Brewfile_utils                             (utility brew dependencies file can be added)
	   +-- config_folder.xml                          (sample configuration file for a Jenkins 'folder')
	   +-- config_pipeline_mobile_parallel_stages.xml (sample configuration file for a mobile pipeline with parallel stages)
	   +-- config_pipeline_mobile_parallel_steps.xml  (sample configuration file for a mobile pipeline with parallel steps)
	   +-- config_pipeline_test_ci.xml                (sample configuration file for test pipeline CICD platform environment)
	   +-- functions.sh                               (utility functions)
	   +-- jenkins_default.yaml                       (default Jenkins configuration sample file to load by 'configuration as code' plugin)
	   +-- jenkins_plugins                            (list of Jenkins plugins installed by scripts)
	   +-- jenkins_tools.sh                           (utility functions for Jenkins)
	   +-- npm_packages                               (list of npm packages installed by scripts)
	+-- shells
	   +-- 00-env.sh                                  (prepare installation environment)
	   +-- 01-homebrew.sh                             (install / check / update homebrew and its tools)
	   +-- 02-xcode.sh                                (install Xcode Command Line Tools and Xcode)
	   +-- 03-git.sh                                  (install Git)
	   +-- 04-node.sh                                 (install Node (with NPM))
	   +-- 05-npm.sh                                  (install / check / update NPM and its packages)
	   +-- 06-java.sh                                 (install and configure JDK 1.8)
	   +-- 07-android-sdk.sh                          (install and configure android SDK and install necessary packages from sdkmanager)
	   +-- 08-haxm.sh                                 (install et configure Haxm)
	   +-- 09-google-chrome.sh                        (install Google Chrome)
	   +-- 10-ruby-RVM.sh                             (install, configure and initialize RVM then install and initialize latest Ruby version)
	   +-- 11-fastlane.sh                             (install / update Fastlane)
	   +-- 12-jenkins.sh                              (install Jenkins then start as a service (from brew))
	   +-- 50-utilities.sh                            (install utilities from Brewfile_utils and check environment brew and cask)
	   +-- 51-sublime-text.sh                         (install Sublime Text 3)
	   +-- 52-zsh.sh                                  (install Ho-My-Zsh)
	+-- installCI                                     (complete CICD platform installation script, see operation below)
	+-- setupCI                                       (complete CICD platform configuration script, see operation below)
	+-- uninstallCI                                   (complete CICD platform uninstallation script, see operation below)

### General operation
#### installCI
This script is a wrapper for all scripts in **shells** directory, it will preconfigure and export functions and environment variables needed for installation **(including proxy configuration)**. Authentication will be requested to be sure to have `sudo` rights available in sub-shells. After confirming installation has started, sub-shells in **shells** directory will all be started in order.

If main installation command did not proceed correctly, then the main script will stop and log an error without continuing installation.
#### Sub-shells
##### 00-env
Prepare installation environment.
This script will check that different profile files exist (otherwise it will create them) and are editable. If one of those profile files needed for the install is not editable then this script will return an error and installation will be stopped.
##### 01-homebrew
Homebrew is a very popular package manager under macOS, you can find more information on [homebrew's website](https://brew.sh/index).

This script will install homebrew if it is not already done, otherwise it will clean current install, check integrity and check updates for it and all installed formulas.

During first installation, `core/Brewfile_core` file is loaded as a bundle and will install the necessary tools for Homebrew but also following dependencies:
1. [`trash`](https://formulae.brew.sh/formula/trash) : allow to put in trash app of your choice (used to uninstall Xcode)
2. [`mas`](https://formulae.brew.sh/formula/mas) : provides utilities (CLI) to managing the AppStore (used to install Xcode)
3. [`Xcode`](https://itunes.apple.com/fr/app/xcode/id497799835?mt=12) : development tool for Apple suites (needed to deploy apps on the AppStore) 

##### 02-xcode
This script will install Xcode Command Line Tools if necessary then configure Xcode by modifying the active directory, which allows to use the `xcodebuild` command and accept license conditions. If Xcode was not installed during bundling of Brewfile_core, then installation will be stopped.
##### 03-git
This script will install Git if needed as a Homebrew formula.
##### 04-node
This script will install Node (with NPM) if necessary as a Homebrew formula.
##### 05-npm
This script will update NPM if necessary, then install all necessary packages from `core/npm_packages` file. If these packages are already installed, check for updates. If NPM is not installed then this script will return an error and installation will be stopped.
##### 06-java
This script will uninstall all installed java versions with `brew cask`, then install java 8 with `brew cask`. It will then fill `JAVA_HOME` environment variable in `~/.bash_profile` if it is not already done.
##### 07-android-sdk
This script will install android SDK if necessary with following dependencies from the `sdkmanager`:
1. `platform-tools`
2. `platforms` latest version directly from `sdkmanager`
3. `build-tools` latest version directly from `sdkmanager`

If the android SDK is to be installed, `ANDROID_HOME` environment variable will be set in `~/.bash_profile`.
##### 08-haxm
This script will install Haxm if necessary and set `INTEL_HAXM_HOME` environment variable in `~/.bash_profile`.
##### 09-google-chrome
This script will install Google Chrome if necessary. Google Chrome is essential for CICD platform because most mobile projects use it in Headless mode to run their unit tests (see karma documentation).
##### 10-ruby-RVM
This script will install RVM if necessary, initialize RVM environment for current shell, add to all profile files (`~/.bash_profile`, `~/.profile` ...) RVM initialization to be able to take account for future Ruby versions that will be installed. Then, Ruby will be installed or updated if necessary in latest version.

**CAUTION: To use Ruby versions installed by RVM, always use a `bash` terminal. Which means that any Jenkins DSL scripts or pipelines will need to consider this point to properly use Fastlane which requires a recent version of Ruby.**
##### 11-fastlane
This script will install or update [Fastlane](https://fastlane.tools/) if necessary.
##### 12-jenkins
This script will install jenkins-lts if needed as Homebrew formulae. It will then start the Jenkins instance as a brew service before providing the available commands for the brew services as well as the brew service list (if install is done on a blank machine then we should only see jenkins-lts service).
##### 50-utilities
This script will install utilities from Brewfile_utils, you can add what you need that is not required for CICD platform.
##### 51-sublime-text
This script will ask if you want to install Sublime Text 3 and install it if answer is yes.
##### 52-zsh
This script will ask if you want to install Ho-My-Zsh and install it if answer is yes.

#### setupCI
This script is independent, uses only the **core** directory content and not the **shells** directory. In the same way as the `installCI` script, it will preconfigure and export functions and environment variables needed for configuration **(including proxy configuration)**. Authentication will be requested to make sure that you have `sudo` rights available during configuration. After confirmation, the CICD platform configuration will begin in the following order:
1. Jenkins conf initialization
    1. Creating and valuing `lastExecVersion` file
    2. Creating and valuing `locationConfiguration` file, default to `http://localhost:8080/`. This script can be modified to pass another value to the corresponding function
    3. Changing `installStateName` from NEW to RUNNING
    4. Fill `JENKINS_URL` environment variable by default to `http://localhost:8080/` retrieved in the file `locationConfiguration`
    5. Export `JENKINS_URL` environment variable
    6. Positioning admin account that will be used by `jenkins_cli` from `JENKINS_USERNAME` and `JENKINS_PASSWORD` environment variables
2. Proxy configuration request, creation of `proxy.xml` file at `JENKINS_HOME` if necessary
3. Install Jenkins plugins in `core/jenkins_plugins` file order
4. Request to add global Jenkins configuration from the **as-code** configuration plugin, using `core/jenkins_default.yaml` default configuration or another configuration that will be asked for the path
5. Add Jenkins folder `CI Test CD` from the `config_folder.xml` file
6. Add pipeline `check_env` from the `config_pipeline_test_ci.xml` file in Jenkins folder `Test CI CD`
7. Add pipeline `pipe_mobile_v1` from the `config_pipeline_mobile_parallel_steps.xml` file to Jenkins root

#### uninstallCI
This script is independent, it only uses the contents of **core** directory and not **shells** directory. In the same way as the `installCI` script, it will preconfigure and export features and environment variables needed to uninstall **(including proxy configuration)**. Authentication will be requested to make sure that you have `sudo` rights available during uninstalling. After confirmation, **jenkins-lts** service is stopped and all the CICD platform is uninstalled in following order:
1. Jenkins
2. NPM packages
3. `cask` dependencies from Brewfile_core
4. `mas` dependencies from Brewfile_core
5. Java 8
6. Android SDK (with sdkmanager dependencies)
7. Haxm
8. Google Chrome
9. RVM (with Ruby)
10. Homebrew dependencies (from Brewfile_core then **git** and **node** if installed from brew)
11. Fastlane

The environment is cleaned up throughout the uninstall process (`.bashrc`, `.bash_profile`, `.profile` ...).

## Prerequisites
The various scripts provided here are intended to work from version **macOS Mojave 10.14.4**, they have however been tested under **macOS High Sierra 10.13.6**, some tools may not behave correctly under a version older than **Mojave**.

Some commands require administrator rights to run, so `installCI`, `setupCI`, and `uninstallCI` scripts ask `sudo` rights.

It is advisable to use these scripts from a logged `bash` terminal with an account that has these rights, otherwise the installation can not be done correctly.

## Use
The following command will create a directory dedicated to CICD platform installation, clone the repo in this directory and then go to created directory.
After removing `.git/` directory, `installCI` script will be launched and installation will start.

`mkdir ~/.installCI && git clone https://gitlab.com/MaxTod/macos-setup-ci.git/ ~/.installCI && cd ~/.installCI && rm -rf .git/ && bash installCI`

You can also start installation, configuration, and uninstall scripts as follows, after cloning this repo in `~/.installCI` directory.
### Installation
`bash ~/.installCI/installCI`
### Configuration
**CAUTION: Installation and configuration of Jenkins instance requires maintenance of configuration as code from `core/jenkins_default.yaml` file as well as the ordered list of jenkins plugins from `core/jenkins_plugins` file. Please update the necessary values in `core/jenkins_default.yaml` file and check the integrity of Jenkins plugins list.**

`bash ~/.installCI/setupCI`
### Uninstall
`bash ~/.installCI/uninstallCI`
