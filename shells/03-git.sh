#!/usr/bin/env bash

echo " 📦  Git"

ending_git_install() {
  GIT_DIR_AND_VERSION="($(which git)) in version '$(git --version)'"
}

if [[ ! -f $(which git) ]]
then
  print_info_installing
  brew install git
  check_previous_command_exec "Git installation"
  ending_git_install
  print_success_completed "Git install ${GIT_DIR_AND_VERSION}"
else
  ending_git_install
  print_success "Git already installed ${GIT_DIR_AND_VERSION}"
fi
