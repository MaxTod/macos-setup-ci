#!/usr/bin/env bash

echo " 📦  NPM packages"

if [[ -f $(which npm) ]]
then
  sudo npm doctor
  NPM_CURRENT_VERSION="$(echo $(npm -v))"
  NPM_LATEST_VERSION="$(echo $(sudo npm view npm versions | grep -v - | sed '/^$/d' | tail -1 | sed -e 's/]//g' -e "s/ *'//g" -e 's/,//g'))"
  if test "${NPM_CURRENT_VERSION}" = "${NPM_LATEST_VERSION}"
  then
    print_success "NPM is up to date"
  else
    print_info "Update NPM ..."
    sudo npm update -g npm
    print_success_completed "NPM updated in version '$(npm -v)'"
  fi
else
  print_error "NPM" "not installed, please rerun CI install script to install npm via brew node install"
  exit 1
fi

cat "${ROOT_DIR}/core/npm_packages" | while read -r PACKAGE
do
  if ! sudo npm list -g --depth=0 | grep -q $PACKAGE
  then
    print_info "🖥  Installing - ${PACKAGE} ..."
    sudo npm install -g $PACKAGE
    check_previous_command_exec "${PACKAGE} installation"
    print_success_completed "NPM package '${PACKAGE}' installed in global node_modules"
  else
    print_info "Package '${PACKAGE}' already installed in global node_modules, check for update ..."
    PACKAGE_CURRENT_VERSION="$(echo $(sudo npm view ${PACKAGE} version))"
    PACKAGE_LATEST_VERSION="$(echo $(sudo npm view ${PACKAGE} versions | grep -v - | sed '/^$/d' | tail -1 | sed -e 's/]//g' -e "s/ *'//g" -e 's/,//g'))"
    if test "${PACKAGE_CURRENT_VERSION}" = "${PACKAGE_LATEST_VERSION}"
    then
      print_success "'${PACKAGE}' is up to date"
    else
      print_info "Update '${PACKAGE}' ..."
      sudo npm update -g $PACKAGE
      print_success_completed "'${PACKAGE}' updated in version '$(sudo npm view ${PACKAGE} version)'"
    fi
  fi
done
