#!/usr/bin/env bash

echo " 📦  Haxm"

ending_haxm_install() {
  HAXM_VERSION="$(find /usr/local/Caskroom/intel-haxm -maxdepth 1 2> /dev/null | sed "s/\/usr\/local\/Caskroom\/intel-haxm\///g" | grep -v "^\." | tail -1)"
  HAXM_DIR="in directory '${HOMEBREW_CASKROOM_PATH}/intel-haxm' in version '${HAXM_VERSION}'"
}

if ! test -d "${HOMEBREW_CASKROOM_PATH}/intel-haxm"
then
  print_info_installing
  print_warning "This might not work on High Sierra !"
  brew cask install intel-haxm
  check_previous_command_exec "HAXM installation"
  print_info "Set INTEL_HAXM_HOME env var ..."
  set_env_var INTEL_HAXM_HOME "${HOMEBREW_CASKROOM_PATH}/intel-haxm"
  ending_haxm_install
  print_success_completed "Haxm install ${HAXM_DIR}"
else
  print_info "INTEL_HAXM_HOME="${INTEL_HAXM_HOME}
  ending_haxm_install
  print_success "Haxm already installed ${HAXM_DIR}"
fi
