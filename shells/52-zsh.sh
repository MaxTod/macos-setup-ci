#!/usr/bin/env bash

echo " 📦  Oh-my-zsh"

install_zsh() {
  print_info_installing

  if [[ ! -d "$HOME/.oh-my-zsh" ]]; then
    curl -L "http://install.ohmyz.sh" | sh
    print_success_completed "Oh-my-zsh install"
  else
    print_info "Oh-my-zsh already installed in directory '$(${HOME}/.oh-my-zsh)'"
  fi

  print_info "Configuring Oh-my-zsh ..."

  if ! grep -Fxq "$CONFIGURED_MESSAGE" "$HOME/.zshrc"
  then
    echo "
  $CONFIGURED_MESSAGE
  if [ -f ~/.my-zshrc ]; then
    . ~/.my-zshrc
  fi" >> "$HOME/.zshrc"
    print_info "Oh-my-zsh configured"
  else
    print_info "Skipping configuring Oh-my-zsh ..."
  fi
  ask_yes_no_question "Do you want to use zsh as default shell?" "chsh -s /bin/zsh" ""
}

skip_install_zsh() {
  print_info "Skipping installing Oh-my-zsh ..."
}

ask_yes_no_question "Do you want to install Oh-my-zsh?" "install_zsh" "skip_install_zsh"
