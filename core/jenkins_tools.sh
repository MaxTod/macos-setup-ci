PWD_PATTERN_RULE_LOWERCASE="[a-z]"
PWD_PATTERN_RULE_LOWERCASE_LABEL="  - one lowercase alphabetical character \n"
PWD_PATTERN_RULE_UPPERCASE="[A-Z]"
PWD_PATTERN_RULE_UPPERCASE_LABEL="  - one uppercase alphabetical character \n"
PWD_PATTERN_RULE_NUMERIC="[0-9]"
PWD_PATTERN_RULE_NUMERIC_LABEL="  - one numeric character \n"
PWD_PATTERN_RULE_SPECIAL="[!@#\$\^\&\*]"
PWD_PATTERN_RULE_SPECIAL_LABEL="  - one special character \n"
PWD_PATTERN_RULE_SIZE="^.{6,}$"
PWD_PATTERN_RULE_SIZE_LABEL="  - 6 characters or longer \n"

check_password_pattern() {
  PWD_TO_VERIFY="$1"
  PWD_PATTERN_ERRORS=()
  if ! [[ ${PWD_TO_VERIFY} =~ ${PWD_PATTERN_RULE_LOWERCASE} ]] ; then
    PWD_PATTERN_ERRORS+="${PWD_PATTERN_RULE_LOWERCASE_LABEL}"
  fi
  if ! [[ ${PWD_TO_VERIFY} =~ ${PWD_PATTERN_RULE_UPPERCASE} ]] ; then
    PWD_PATTERN_ERRORS+="${PWD_PATTERN_RULE_UPPERCASE_LABEL}"
  fi
  if ! [[ ${PWD_TO_VERIFY} =~ ${PWD_PATTERN_RULE_NUMERIC} ]] ; then
    PWD_PATTERN_ERRORS+="${PWD_PATTERN_RULE_NUMERIC_LABEL}"
  fi
  if ! [[ ${PWD_TO_VERIFY} =~ ${PWD_PATTERN_RULE_SPECIAL} ]] ; then
    PWD_PATTERN_ERRORS+="${PWD_PATTERN_RULE_SPECIAL_LABEL}"
  fi
  if ! [[ ${PWD_TO_VERIFY} =~ ${PWD_PATTERN_RULE_SIZE} ]] ; then
    PWD_PATTERN_ERRORS+="${PWD_PATTERN_RULE_SIZE_LABEL}"
  fi
  if [ ${#PWD_PATTERN_ERRORS[@]} -eq 0 ] ; then
    return 1
  else
    print_warning "Please, password must contain at least : \n${PWD_PATTERN_ERRORS}"
    return 0
  fi
}

jenkins__check_health() {
  jenkins_cli "version" &> /dev/null
  if test $? -eq 0 ; then
    print_info "Service 'jenkins-lts' fully started"
    return 0
  else
    print_warning "Service 'jenkins-lts' not completely started, please wait ..."
    return 1
  fi
}

jenkins__check_version() {
  if test -d ${JENKINS_HOME} && [[ $(brew list | grep jenkins-lts) ]]
  then
    JENKINS_VERSION=$(grep "<version>" ${JENKINS_CONFIG_FILE} | sed -e 's/ //g' -e 's/.*<version>//g' -e 's/<\/version>.*//g')
    JENKINS_DIR_AND_VERSION="in directory '${JENKINS_HOME}', in version '${JENKINS_VERSION}'"
    print_info "Jenkins installed ${JENKINS_DIR_AND_VERSION}"
  else
    print_error "Jenkins" "not installed, please rerun CI install script to install jenkins-lts via brew"
    exit 1
  fi
}

jenkins_safe_restart() {
  brew services restart jenkins-lts
  until jenkins__check_health ; do
      sleep 3
  done
}

jenkins_cli() {
  JENKINS_ADMIN_USER="${2:-$JENKINS_USERNAME}"
  JENKINS_ADMIN_PASSWORD="${3:-$JENKINS_PASSWORD}"
  java -jar ${JENKINS_CLI} -auth ${JENKINS_ADMIN_USER}:${JENKINS_ADMIN_PASSWORD} $1
  #java -jar ~/.jenkins/war/WEB-INF/jenkins-cli.jar -auth ${JENKINS_ADMIN_USER}:${JENKINS_ADMIN_PASSWORD} $1
}

jenkins__add_admin_user() {
  JENKINS_USERNAME_PATTERN="^[a-z0-9_-]{3,20}$"
  while true; do
    ask "You will create Jenkins admin user. Please, enter login :"
    if [[ ${REPLY} =~ ${JENKINS_USERNAME_PATTERN} ]] ; then
      JENKINS_USERNAME_TO_ADD=${REPLY}
      print_info "Login : ${JENKINS_USERNAME_TO_ADD}"
      print_info "A password must containing at least : \n${PWD_PATTERN_RULE_LOWERCASE_LABEL}${PWD_PATTERN_RULE_UPPERCASE_LABEL}${PWD_PATTERN_RULE_NUMERIC_LABEL}${PWD_PATTERN_RULE_SPECIAL_LABEL}${PWD_PATTERN_RULE_SIZE_LABEL}"
      while true; do
        ask_hidden "Please, enter password :"
        check_password_pattern ${REPLY}
        if test $? -eq 1 ; then
          JENKINS_PASSWORD_TO_ADD=${REPLY}
          while true; do
            ask_hidden "Please, confirm password : "
            if [[ ${JENKINS_PASSWORD_TO_ADD} == ${REPLY} ]] ; then
              print_info "Jenkins admin user '${JENKINS_USERNAME_TO_ADD}' will be created ..."
              echo "jenkins.model.Jenkins.instance.securityRealm.createAccount(\"${JENKINS_USERNAME_TO_ADD}\", \"${JENKINS_PASSWORD_TO_ADD}\")" | jenkins_cli "groovy ="
              export JENKINS_USERNAME=${JENKINS_USERNAME_TO_ADD}
              export JENKINS_PASSWORD=${JENKINS_PASSWORD_TO_ADD}
              break
            else
              print_warning "Wrong password"
            fi
          done
          break
        fi
      done
      break
    else
      print_warning "Please match username on pattern '${JENKINS_USERNAME_PATTERN}'"
    fi
  done
  jenkins_safe_restart
}

jenkins__encrypt_password() {
  PASSWORD_TO_ENCRYPT="$1"
  echo "println(hudson.util.Secret.fromString(\"${PASSWORD_TO_ENCRYPT}\").getEncryptedValue())" | jenkins_cli "groovy ="
}

jenkins__decrypt_password() {
  PASSWORD_TO_DECRYPT="$1"
  echo "println(hudson.util.Secret.decrypt(\"${PASSWORD_TO_DECRYPT}\"))" | jenkins_cli "groovy ="
}

jenkins__enable_https() {
  replace_in_file "http:\/\/updates.jenkins.io\/update-center.json" "https:\/\/updates.jenkins.io\/update-center.json" "${JENKINS_UPDATE_CENTER_FILE}"
  print_info "Jenkins https enabled"
}

jenkins__disable_https() {
  replace_in_file "https:\/\/updates.jenkins.io\/update-center.json" "http:\/\/updates.jenkins.io\/update-center.json" "${JENKINS_UPDATE_CENTER_FILE}"
  print_info "Jenkins https disabled"
}

jenkins__update_installStateName_to_RUNNING() {
  replace_in_file "<installStateName>.*<" "<installStateName>RUNNING<" "${JENKINS_CONFIG_FILE}"
  print_info "Jenkins installStateName updated to 'RUNNING'"
}

jenkins__set_lastExecVersion_file() {
  # call jenkins__check_version before
  create_file "${JENKINS_LAST_EXEC_VERSION_FILE}" "${JENKINS_VERSION}"
}

jenkins__set_locationConfiguration_file() {
  JENKINS_URL_PARAM="$1"
  create_file "${JENKINS_LOCATION_CONFIGURATION_FILE}" "<?xml version='1.1' encoding='UTF-8'?>\n<jenkins.model.JenkinsLocationConfiguration>\n  <jenkinsUrl>$(echo ${JENKINS_URL_PARAM:-"http://localhost:8080/"})</jenkinsUrl>\n</jenkins.model.JenkinsLocationConfiguration>"
}

jenkins__set_proxy() {
  PROXY_NAME=$(echo $1)
  PROXY_PORT=$(echo ${2:-"8080"})
  PROXY_USERNAME=$(echo $3)
  PROXY_PASSWORD=$(jenkins__encrypt_password "$4")
  PROXY_NO_PROXY_HOST=$(echo ${5:-"localhost"})
  create_file "${JENKINS_PROXY_FILE}" "<?xml version='1.1' encoding='UTF-8'?>\n<proxy>\n  <name>${PROXY_NAME}</name>\n  <port>${PROXY_PORT}</port>\n  <userName>${PROXY_USERNAME}</userName>\n  <noProxyHost>${PROXY_NO_PROXY_HOST}</noProxyHost>\n  <secretPassword>${PROXY_PASSWORD}</secretPassword>\n</proxy>"
  jenkins_safe_restart
}

jenkins_proceed_proxy_configuration() {
  ask "Please, enter proxy name : "
  PROXY_NAME="${REPLY}"
  ask "Please, enter proxy port : "
  PROXY_PORT="${REPLY}"
  ask "Please, enter proxy user : "
  PROXY_USERNAME="${REPLY}"
  ask_hidden "Please, enter proxy password : "
  PROXY_PASSWORD="${REPLY}"
  ask "Please, enter no proxy host : "
  PROXY_NO_PROXY_HOST="${REPLY}"
  print_info "Adding Jenkins proxy configuration ..."
  jenkins__set_proxy "${PROXY_NAME}" "${PROXY_PORT}" "${PROXY_USERNAME}" "${PROXY_PASSWORD}" "${PROXY_NO_PROXY_HOST}"
}

jenkins_skip_proxy_configuration() {
  #jenkins__disable_https
  print_info "Jenkins proxy configuration skipped ..."
}

jenkins__install_plugin() {
  JENKINS_PLUGIN_NAME="$1"
  JENKINS_PLUGIN_BASEURL="http://updates.jenkins.io/latest/"
  JENKINS_PLUGIN_EXTENSION=".hpi"
  # INFO -noCertificateCheck ne devrai pas être utilisé, passer par la configuration d'un proxy
  jenkins_cli "install-plugin -deploy "${JENKINS_PLUGIN_BASEURL}${JENKINS_PLUGIN_NAME}${JENKINS_PLUGIN_EXTENSION}""
}

jenkins_install_plugins() {
  while read -r -u 10 PLUGIN
  do
    jenkins_cli "list-plugins" | grep "${PLUGIN}\s" &> /dev/null
    if test $? -eq 1 ; then
      print_info "🖥  Installing - ${PLUGIN} ..."
      jenkins__install_plugin "${PLUGIN}"
      if test $? -eq 1 ; then
        print_error "Plugin" "'${PLUGIN}' install error"
      else
        print_success_completed "Jenkins plugin '${PLUGIN}' install"
      fi
    else
      print_info "Jenkins plugin '${PLUGIN}' already installed"
    fi
  done 10<"${ROOT_DIR}/core/jenkins_plugins"
  jenkins_safe_restart
}

jenkins__add_default_configuration() {
  cp "$ROOT_DIR/core/jenkins_default.yaml" "${CASC_JENKINS_CONFIG}"
}

jenkins__add_new_configuration() {
  ask "Please, enter yaml configuration file full path : "
  CASC_JENKINS_CONFIG_PATH="${REPLY}"
  cp "${CASC_JENKINS_CONFIG_PATH}" "${CASC_JENKINS_CONFIG}"
}

jenkins_add_configuration() {
  mkdir "${JENKINS_HOME}/casc_configs"
  ask_yes_no_question "Do you want to use default configuration (${ROOT_DIR}/core/jenkins_default.yaml)?" "jenkins__add_default_configuration" "jenkins__add_new_configuration"
  jenkins__validate_casc
}

jenkins__validate_casc() {
  CASC_JENKINS_CONFIG_PARAM=$(echo ${1:-$CASC_JENKINS_CONFIG})
  JENKINS_CRUMB=$(curl -c cookies -s -u "${JENKINS_USERNAME}:${JENKINS_PASSWORD}" "${JENKINS_URL}crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)")
  curl -b cookies -s -X POST -u "${JENKINS_USERNAME}:${JENKINS_PASSWORD}" -H "${JENKINS_CRUMB}" -d "newSource=$(urlencode "${CASC_JENKINS_CONFIG_PARAM}")" "${JENKINS_URL}configuration-as-code/checkNewSource" | grep "The configuration can be applied" &> /dev/null
  if test $? -eq 0 ; then
    print_info "The configuration will be applied ..."
    jenkins__apply_casc "${CASC_JENKINS_CONFIG_PARAM}"
  else
    print_error "'${CASC_JENKINS_CONFIG_PARAM}'" "configuration cannot be applied"
  fi
}

jenkins__apply_casc() {
  CASC_JENKINS_CONFIG_PARAM=$(echo ${1:-$CASC_JENKINS_CONFIG})
  RESPONSE_STATUS=$(curl -b cookies -s -X POST -w %{http_code} -u "${JENKINS_USERNAME}:${JENKINS_PASSWORD}" -H "${JENKINS_CRUMB}" -d "_.newSource=$(urlencode "${CASC_JENKINS_CONFIG_PARAM}")" "${JENKINS_URL}configuration-as-code/replace")
  if test ${RESPONSE_STATUS} -eq 302 ; then
    print_success "The configuration has been applied"
    jenkins_cli "disable-plugin configuration-as-code"
  else
    print_error "'${CASC_JENKINS_CONFIG_PARAM}'" "configuration has not been applied"
  fi
  rm -f cookies;
}
