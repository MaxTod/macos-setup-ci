#!/usr/bin/env bash

echo " 📦  Sublime Text"

install_sublime_text() {
  DIR=$(echo "$HOME/Library/Application Support/Sublime Text 3")

  print_info "🖥  Installing package control ..."

  if [ ! -f "$DIR/Installed Packages/Package Control.sublime-package" ]
  then
    osascript -e 'quit app "Sublime Text"'

    curl -o "$DIR/Installed Packages/Package Control.sublime-package" \
            "https://packagecontrol.io/Package Control.sublime-package"

    print_success_completed "Sublime Text install"
  else
    print_info "Skipping ..."
  fi
}

skip_install_sublime_text() {
  print_info "Skipping installing Sublime Text ..."
}

ask_yes_no_question "Do you want to install Sublime Text?" "install_sublime_text" "skip_install_sublime_text"
