#!/usr/bin/env bash

echo " 📦  Jenkins"

ending_jenkins_install() {
  JENKINS_DIR="in directory '${JENKINS_HOME}'"
  print_info "Go to following URL to open Jenkins dashboard : http://localhost:8080"
}

if ! test -d ${JENKINS_HOME} && [[ ! $(brew list | grep jenkins-lts) ]]
then
  print_info_installing
  brew install jenkins-lts
  check_previous_command_exec "Jenkins installation"
  brew services start jenkins-lts
  print_info "You can use following commands with brew services (eg: brew services start jenkins-lts) : "
  brew services --help
  brew services list
  ending_jenkins_install
  print_success_completed "Jenkins install ${JENKINS_DIR}"
else
  ending_jenkins_install
  print_success "Jenkins already installed ${JENKINS_DIR}"
fi
