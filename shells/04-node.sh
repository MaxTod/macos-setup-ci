#!/usr/bin/env bash

echo " 📦  Node (with NPM)"

ending_node_install() {
  NODE_DIR_AND_VERSION="($(which node)) in version '$(node --version)'"
}

if [[ ! -f $(which node) ]]
then
  print_info_installing
  brew install node
  check_previous_command_exec "Node installation"
  ending_node_install
  print_success_completed "Node install ${NODE_DIR_AND_VERSION}"
else
  ending_node_install
  print_success "Node is already installed ${NODE_DIR_AND_VERSION}"
fi
