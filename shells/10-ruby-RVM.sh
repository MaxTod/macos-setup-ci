#!/usr/bin/env bash

echo " 📦  Ruby (RVM)"

RVM_SCRIPT="$(echo ~/.rvm/scripts/rvm)"

check_ruby_env() {
  type rvm | head -n 1
  rvm -v
  ruby -v
  gem env
}

ending_rvm_install() {
  print_info "Sourcing '${RVM_SCRIPT}' ..."
  source ${PROFILE}
  RUBY_LATEST_VERSION=$(rvm list known | grep -e "\[ruby-\]" | grep -v "preview" | tail -1 | sed -e "s/\[//g" -e "s/\]//g")
  RVM_DIR_AND_VERSION="in directory '$(which rvm)' in version '$(rvm -v)'"
}

install_latest_ruby_version() {
  print_info "🖥  Installing - Ruby ${RUBY_LATEST_VERSION}"
  rvm install ${RUBY_LATEST_VERSION}
  check_previous_command_exec "Ruby installation"
  print_success_completed "Ruby ${RUBY_LATEST_VERSION} install with RVM"
  print_info "Set default Ruby version to ${RUBY_LATEST_VERSION}"
  rvm use ${RUBY_LATEST_VERSION} --default
}

update_ruby_version() {
  RUBY_RVM_CURRENT_VERSION=$(rvm list strings | tail -1)
  if test "${RUBY_RVM_CURRENT_VERSION}" = "${RUBY_LATEST_VERSION}"
  then
    print_success "Ruby version is up to date"
  else
    print_warning "Ruby needs to be updated, update it ..."
    install_latest_ruby_version
  fi
}

if [[ ! -f $(which rvm) ]]
then
  print_info_installing
  curl -L https://get.rvm.io | bash -s stable
  #curl -sSL https://get.rvm.io | bash -s stable --ruby #To install rvm directly with ruby
  ending_rvm_install
  print_success_completed "RVM install ${RVM_DIR_AND_VERSION}"
  install_latest_ruby_version
else
  ending_rvm_install
  print_success "RVM already installed ${RVM_DIR_AND_VERSION}"
  update_ruby_version
fi

check_ruby_env
