#!/usr/bin/env bash

echo " 📦  Fastlane"

# To load rvm
. ${BASH_PROFILE}

if [[ ! -f $(which fastlane) ]]
then
  print_info_installing
  brew cask install fastlane
  check_previous_command_exec "Fastlane installation"
  print_info "Configure Fastlane environment ..."
  export PATH="~/.fastlane/bin:$PATH"
  print_success_completed "Fastlane install in version $(fastlane -v)"
else
  print_info "Fastlane, check for update ..."
  brew cask upgrade fastlane
  print_success "Fastlane already installed in version $(fastlane -v)"
fi
