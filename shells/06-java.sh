#!/usr/bin/env bash

echo " 📦  Java 8"

JAVA_BREW_CASK_INSTALL_PATH_PREFIX="/Library/Java/JavaVirtualMachines"
JAVA_8_HOME_PATH=$(/usr/libexec/java_home -v 1.8)

set_java_home() {
  set_env_var JAVA_HOME "\$(/usr/libexec/java_home -v 1.8)"
  print_info "JAVA_HOME="${JAVA_HOME}
}

${JAVA_8_HOME_PATH} &> /dev/null
if ! (test $? -eq 0 && test -d ${JAVA_8_HOME_PATH}) ||  [[ -z "${JAVA_8_HOME_PATH}" ]]
then
  print_info_installing
  brew cask uninstall java
  brew cask uninstall java8
  brew cask install homebrew/cask-versions/adoptopenjdk8
  check_previous_command_exec "Java 8 installation"
  print_info "Set JAVA_HOME env var ..."
  set_java_home
  java -version
  print_success_completed "Java 8 install in directory '${JAVA_HOME}'"
else
  print_success "Java 8 already installed in directory '${JAVA_8_HOME_PATH}', check JAVA_HOME env var ..."
  LATEST_JDK_PATH=$(find /Library/Java/JavaVirtualMachines -maxdepth 1 -type d | tail -1)
  if test "${LATEST_JDK_PATH}" = "${JAVA_BREW_CASK_INSTALL_PATH_PREFIX}/jdk1.8."*.jdk
  then
    if test "${LATEST_JDK_PATH}/Contents/Home" = "${JAVA_8_HOME_PATH}" &&
       test ! -z "${JAVA_HOME}" &&
       test "${JAVA_HOME}" = "${JAVA_8_HOME_PATH}"
    then
      print_success "Java 8 home path is correctly valued"
      print_info "JAVA_HOME="${JAVA_HOME}
    else
      print_warning "Java 8 home path is not correctly valued, update JAVA_HOME env var ..."
      set_java_home
    fi
  fi
  java -version
  print_success_completed "Java 8 env configuration"
fi
