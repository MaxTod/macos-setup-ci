#!/usr/bin/env bash

echo " 📦  Utilities from Homebrew, Cask and mas"

install_utilities() {
  print_info_installing
  brew bundle --file="$ROOT_DIR/core/Brewfile_utils"
  brew cleanup && brew upgrade && brew update && brew doctor -v && brew cask doctor
  print_success_completed "utilities install"
}

skip_install_utilities() {
  print_info "Skipping installing utilities ..."
}

ask_yes_no_question "Do you want to install the utilities?" "install_utilities" "skip_install_utilities"
