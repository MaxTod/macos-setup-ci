#!/usr/bin/env bash

echo " ⚙️  Env"

check_profile_file() {
  if [ ! -e "$1" ]
  then
    print_info "'$1' file does not exist, creating ..."
    touch "$1"
  elif test "$1" = "${BASH_PROFILE}"
  then
    print_info "'$1' file exists, sourcing ..."
    . $1
  fi

  if [ ! -w "$1" ]
  then
    print_error "'$1'" "file does not writable ... You need to update his rights or use admin account"
    exit 1
  else
    print_info "'$1' file writables"
  fi
}

check_profile_file "${BASHRC}"
check_profile_file "${PROFILE}"
check_profile_file "${MKSHRC}"
check_profile_file "${ZLOGIN}"
check_profile_file "${ZSHRC}"
check_profile_file "${BASH_PROFILE}"

# Prepare env vars for fastlane
set_env_var LC_ALL "fr_FR.UTF-8"
set_env_var LANG "fr_FR.UTF-8"

print_success_completed "env configuration"
