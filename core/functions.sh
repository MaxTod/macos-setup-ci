check_for_login_shell() {
  shopt -q login_shell
  if test $? -eq 0 ; then
    print_success 'Login shell'
  else
    print_info 'Not login shell'
    print_error "Current shell" "is not a login shell, please try to restart script with option -l (eg: 'sh -l ~/.installCI/installCI') to start in login shell"
    exit 1
  fi
}

check_previous_command_exec() {
  STATUS=$?
  PREVIOUS_COMMAND_NAME="$1"
  if test ${STATUS} -eq 1
  then
    print_error "${PREVIOUS_COMMAND_NAME}" "running with error, please resolve it and retry install"
    exit 1
  fi
}

answer_is_yes() {
  [[ "$REPLY" =~ ^[Yy]$ ]] && return 0 || return 1
}

ask() {
  print_question "$1"
  read
}

ask_hidden() {
  print_question "$1"
  read -s
}

ask_for_confirmation() {
  print_question "$1 (y/n) "
  read -n 1
  printf "\n"
}

ask_for_sudo() {
  # Ask for the administrator password upfront
  sudo -v

  # Update existing `sudo` time stamp until this script has finished
  # https://gist.github.com/cowboy/3118588
  while true; do
    sudo -n true
    sleep 60
    kill -0 "$$" || exit
  done &> /dev/null &
}

ask_yes_no_question() {
  while true; do
    ask_for_confirmation "$1"
    case $REPLY in
      [Yy]* ) $2 ; break;;
      [Nn]* ) $3 ; break;;
      * ) echo "Please answer, y (yes) or n (no)"
    esac
  done
}

print_info() {
  # Print output in blue
  printf "\n\e[0;34m ℹ️  $1\e[0m\n"
}

print_question() {
  # Print output in purple
  printf "\n\e[0;35m 🤔  $1\e[0m\n"
}

print_success() {
  # Print output in green
  printf "\n\e[0;32m ✅ $1\e[0m\n"
}

print_warning() {
  # Print output in yellow
  printf "\e[0;33m ⚠️  $1\e[0m\n"
}

print_error() {
  # Print output in red
  printf "\n\e[0;31m ⛔️  $1 $2\e[0m\n"
}

print_info_installing() {
  print_info "🖥  Installing ..."
}

print_info_uninstalling() {
  print_info "🖥  Uninstalling $1 ..."
}

print_success_completed() {
  print_success "Completed $1"
}

source_if_is_bash_profile() {
  FILE_ARG="$1"
  # Source only ~/.bash_profile
  if test "${FILE_ARG}" = "${BASH_PROFILE}"
  then
    print_info "Sourcing '${FILE_ARG}' ..."
    . ${FILE_ARG}
  fi
}

##############################################################################################################################
########## Add env var
###### $1 : name of env var to add/set
###### $2 : value of env var to add/set
###### Add or replace env var by name ($1) in selected PROFILE_FILE and source ~/.bash_profile
###### This func use a tmp file (.[profile].tmp) to work
##############################################################################################################################
set_env_var_core() {
  PROFILE_FILE=$3
  ENVVAR_VALUE=$2
  ENVVAR_NAME="$1"
  ENVVAR="$1=\"$2\""
  TMP_EXT=".tmp"
  rm -f ${PROFILE_FILE}${TMP_EXT}
  grep "${ENVVAR}" ${PROFILE_FILE} &> /dev/null
  if test $? -eq 1 && test -d $(eval ${ENVVAR_VALUE} &> /dev/null) ; then
    print_info "${ENVVAR_NAME} env var is not correctly set in '${PROFILE_FILE}', set it correctly ..."
    grep ${ENVVAR_NAME} ${PROFILE_FILE} &> /dev/null
    if [ $? -eq 1 ]; then
      echo "export ${ENVVAR}" >> ${PROFILE_FILE}
    else
      cp ${PROFILE_FILE} ${PROFILE_FILE}${TMP_EXT}
      grep -v ${ENVVAR_NAME} ${PROFILE_FILE} > ${PROFILE_FILE}${TMP_EXT}
      echo "export ${ENVVAR}" >> ${PROFILE_FILE}${TMP_EXT}
      rm -f ${PROFILE_FILE}
      mv ${PROFILE_FILE}${TMP_EXT} ${PROFILE_FILE}
    fi
  elif test -d $(eval ${ENVVAR_VALUE} &> /dev/null) ; then
    print_success "${ENVVAR_NAME} env var is correctly set in '${PROFILE_FILE}' has : ${ENVVAR_VALUE}"
  else
    print_error "${ENVVAR_NAME}" "env var cannot be set in '${PROFILE_FILE}', no such file or directory to ${ENVVAR_VALUE}"
  fi
  source_if_is_bash_profile ${PROFILE_FILE}
}

unset_env_var_core() {
  PROFILE_FILE=$2
  ENVVAR_NAME="$1"

  grep "${ENVVAR_NAME}" ${PROFILE_FILE} &> /dev/null

  if test $? -eq 0 ; then
    print_info "'${ENVVAR_NAME}' env var found in '${PROFILE_FILE}', unset it ..."
    sed -i '' "/$ENVVAR_NAME/d" ${PROFILE_FILE}
  else
    print_warning "'${ENVVAR_NAME}' env var cannot be found in '${PROFILE_FILE}'"
  fi
  source_if_is_bash_profile ${PROFILE_FILE}
}

set_env_var() {
  set_env_var_core $1 "$2" "${ZSHRC}"
  set_env_var_core $1 "$2" "${BASH_PROFILE}"
}

unset_env_var() {
  unset_env_var_core $1 "${ZSHRC}"
  unset_env_var_core $1 "${BASH_PROFILE}"
}

set_env_line() {
  PROFILE_FILE=$2
  ENVVAR_LINE="$1"

  grep "${ENVVAR_LINE}" ${PROFILE_FILE} &> /dev/null

  if test $? -eq 0 ; then
    print_info "'${ENVVAR_LINE}' env line found in '${PROFILE_FILE}'"
  else
    print_info "'${ENVVAR_LINE}' env line not found in '${PROFILE_FILE}', append it to '${PROFILE_FILE}' ..."
    echo "$ENVVAR_LINE" >> "${PROFILE_FILE}"
  fi
  source_if_is_bash_profile ${PROFILE_FILE}
}

remove_pattern_line_from_envfile() {
  PATTERN="$1"
  FILE=$2

  grep "${PATTERN}" ${FILE} &> /dev/null

  if test $? -eq 0 ; then
    PATTERN_IN_FILE=$(grep "${PATTERN}" ${FILE} 2> /dev/null)
    print_info "'${PATTERN}' pattern found in file ${FILE}, remove following line(s) : \n${PATTERN_IN_FILE}"
    sed -i '' "/$PATTERN/d" ${FILE}
  else
    print_warning "'${PATTERN}' pattern cannot be found in file ${FILE}"
  fi
  if [ ! -e "$FILE" ]
  then
    print_warning "'${FILE}' file does not exist"
  else
    source_if_is_bash_profile ${FILE}
  fi
}

urlencode() {
  # urlencode <string>

  local length="${#1}"
  for (( i = 0; i < length; i++ )); do
      local c="${1:i:1}"
      case $c in
          [a-zA-Z0-9.~_-]) printf "$c" ;;
          *) printf '%s' "$c" | xxd -p -c1 |
                 while read c; do printf '%%%s' "$c"; done ;;
      esac
  done
}

urldecode() {
  # urldecode <string>

  local url_encoded="${1//+/ }"
  printf '%b' "${url_encoded//%/\\x}"
}

create_file() {
  FILE_FULL_NAME="$1"
  FILE_CONTENT="$2"
  if test ! -f ${FILE_FULL_NAME}
  then
    print_info "Create '${FILE_FULL_NAME}' file ..."
    touch ${FILE_FULL_NAME}
    print_info "Add following content to file '${FILE_FULL_NAME}' : \n$(echo ${FILE_CONTENT})"
    sh -c "echo '${FILE_CONTENT}\c' > ${FILE_FULL_NAME}"
  else
    print_info "'${FILE_FULL_NAME}' file already exists, do nothing"
  fi
}

replace_in_file() {
  PATTERN_TO_REPLACE="$1"
  REPLACEMENT_PATTERN="$2"
  FILE_TO_UPDATE="$3"

  grep "${PATTERN_TO_REPLACE}" ${FILE_TO_UPDATE} &> /dev/null
  if test $? -eq 0 ; then
    PATTERN_IN_FILE=$(grep "${PATTERN_TO_REPLACE}" ${FILE_TO_UPDATE} 2> /dev/null)
    print_info "'${PATTERN_TO_REPLACE}' pattern found in file ${FILE_TO_UPDATE}, update following line(s) : \n $(echo ${PATTERN_IN_FILE}) \nto \n $(echo ${PATTERN_IN_FILE} | sed "s/$PATTERN_TO_REPLACE/$REPLACEMENT_PATTERN/g")"
    sed -i '' "s/$PATTERN_TO_REPLACE/$REPLACEMENT_PATTERN/g" ${FILE_TO_UPDATE}
  else
    print_warning "'${PATTERN_TO_REPLACE}' pattern cannot be found in file ${FILE_TO_UPDATE}"
  fi
}
